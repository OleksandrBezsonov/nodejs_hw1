const testFolder = './files/';
const fs = require('fs');
const EXTENSIONS = ['.txt', '.yaml', '.log', '.json', '.xml'];
let path = require('path');

function createFile (req, res, next) {
  const filename = req.body.filename;
  const content = req.body.content;
  if (!filename) return res.status(400).json({"message": "Please specify 'filename' parameter"});
  else if (!content) return res.status(400).json({"message": "Please specify 'content' parameter"});
  if(EXTENSIONS.includes(path.extname(filename).toLowerCase())){
  fs.writeFile(testFolder + filename, content, function (err) {
    if (err) {
      throw err;
    } 
    console.log('File is created successfully.');
  });
  } else return res.status(400).json({"message": "Please specify valid file extension parameter"});
  res.status(200).send({ 
    "message": `File created successfully`, 
  });
}

function getFiles (req, res, next) {
  const files = fs.readdirSync(testFolder);
  const targetFiles = files.filter(function(file) {
    return EXTENSIONS.includes(path.extname(file).toLowerCase());
  });
  res.status(200).send({
    "message": "Success",
    "files": targetFiles});
}

const getFile = (req, res, next) => {
  let filename = req.params.filename;
  if (!filename) {
    return res.status(400).json({"message": "No file"});
  }
  const filePath = testFolder + filename;
  if (!fs.existsSync(filePath)) {
    return res.status(400).json({"message": "No file"});
  }
  const extension = path.extname(filename).toLowerCase().substring(1);
  const content = fs.readFileSync(filePath, {
    encoding: 'utf8',
    flag: 'r',
  });
  const { birthtime: uploadedDate } = fs.statSync(filePath);
  return res.status(200).send({
    message: 'Success',
    filename,
    content,
    extension,
    uploadedDate,
  });
};

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
